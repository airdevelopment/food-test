//
//  Meals.h
//  Food-test
//
//  Created by Eugeniya Pervushina on 18/5/16.
//  Copyright © 2016 Air. All rights reserved.
//

#import <CoreData/CoreData.h>
#import <Foundation/Foundation.h>

//@class Categories;

@interface Meals : NSManagedObject

@property (nonatomic, retain) NSString *categories; //Categories
@property (nonatomic, retain) NSString *image_name;
@property (nonatomic, retain) NSString *meals_description;
@property (nonatomic, retain) NSString *title;
@property (nonatomic, retain) NSNumber *nuts;
@property (nonatomic, retain) NSNumber *vegetarian;
@property (nonatomic, retain) NSNumber *price;

@end
