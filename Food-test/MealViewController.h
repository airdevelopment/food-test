//
//  MealViewController.h
//  Food-test
//
//  Created by Eugeniya Pervushina on 17/5/16.
//  Copyright © 2016 Air. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MealTVCell.h"

@interface MealViewController : UIViewController

@property (nonatomic) NSString *category;

@end
