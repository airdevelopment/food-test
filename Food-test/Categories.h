//
//  Categories.h
//  Food-test
//
//  Created by Eugeniya Pervushina on 18/5/16.
//  Copyright © 2016 Air. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface Categories : NSManagedObject

@property (nonatomic, retain) NSString *category;
@property (nonatomic, retain) NSString *image_name;

@end