//
//  Meals.m
//  Food-test
//
//  Created by Eugeniya Pervushina on 18/5/16.
//  Copyright © 2016 Air. All rights reserved.
//

#import "Meals.h"
#import "Categories.h"

@implementation Meals

@dynamic image_name;
@dynamic categories;
@dynamic meals_description;
@dynamic title;
@dynamic nuts;
@dynamic vegetarian;
@dynamic price;

@end
