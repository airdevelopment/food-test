//
//  MealViewController.m
//  Food-test
//
//  Created by Eugeniya Pervushina on 17/5/16.
//  Copyright © 2016 Air. All rights reserved.
//

#import "MealViewController.h"
#import "Meals.h"
#import "AppDelegate.h"
#import "OrderFooterView.h"
#import "OrderViewController.h"

@interface MealViewController () <UITableViewDelegate, UITableViewDataSource, NSFetchedResultsControllerDelegate> {
    NSMutableArray *countArray;
    NSMutableArray *ordersArray;
    
    NSInteger orderCount;
}

@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic) IBOutlet OrderFooterView *footerView;

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, strong) NSManagedObjectContext* managedObjectContext;

@end

@implementation MealViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self loadSettings];
    self.navigationItem.title = _category;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)loadSettings {
    id appDelegate = [[UIApplication sharedApplication]delegate];
    self.managedObjectContext = [appDelegate managedObjectContext];
    self.fetchedResultsController.delegate = self;
    
    _footerView.hidden = YES;
    _footerView.priceLabel.text = @"0";
    
    NSInteger count = [_fetchedResultsController.fetchedObjects count];
    countArray = [[NSMutableArray alloc] init];
    for (int i = 0; i < count; i++) {
        [countArray addObject:@(1)];
    }
    orderCount = 0;
    ordersArray = [[NSMutableArray alloc] init];
}

#pragma mark - TableView
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    id  sectionInfo =
    [[_fetchedResultsController sections] objectAtIndex:section];
    return [sectionInfo numberOfObjects];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Meals *meal = [_fetchedResultsController objectAtIndexPath:indexPath];
    
    MealTVCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    if (cell == nil)
    {
        cell = [[MealTVCell alloc] initWithStyle:UITableViewCellStyleDefault
                                       reuseIdentifier:@"Cell"];
    }
    cell.titleLabel.text = meal.title;
    int num = [countArray[indexPath.row] intValue];
    cell.countLabel.text = [NSString stringWithFormat:@"%i", num];
    
    cell.priceLabel.text = [NSString stringWithFormat:@"%@", meal.price];
    cell.textView.text = meal.meals_description;
    
    cell.foodImageView.image = [UIImage imageNamed:meal.image_name];
    
    if ([meal.nuts isEqualToNumber:@(0)]) {
        cell.nutsView.hidden = YES;
    }
    if ([meal.vegetarian isEqualToNumber:@(0)]) {
        cell.veganView.hidden = YES;
    }
    
    cell.plusButton.tag = indexPath.row;
    cell.minusButton.tag = indexPath.row;
    cell.orderButton.tag = indexPath.row;
    [cell.plusButton addTarget:self action:@selector(plusButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [cell.minusButton addTarget:self action:@selector(minusButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [cell.orderButton addTarget:self action:@selector(orderButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 255;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath  {
    [self performSegueWithIdentifier:@"toOrders" sender:indexPath];
}

#pragma mark - buttons methods
- (void)plusButtonPressed:(UIButton *)button {
    NSInteger num = [countArray[button.tag] integerValue];
    num += 1;
    [countArray replaceObjectAtIndex:(NSUInteger)button.tag withObject:[NSNumber numberWithInteger:num]];
    [self.tableView reloadData];
}

- (void)minusButtonPressed:(UIButton *)button {
    NSInteger num = [countArray[button.tag] integerValue];
    if (num > 0) {
        num -= 1;
        [countArray replaceObjectAtIndex:(NSUInteger)button.tag withObject:[NSNumber numberWithInteger:num]];
        [self.tableView reloadData];
    } else {
        _footerView.hidden = YES;
    }
}

- (void)orderButtonPressed:(UIButton *)button {
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:button.tag inSection:0];
    Meals *meal = [_fetchedResultsController objectAtIndexPath:indexPath];
    
    _footerView.hidden = NO;
    NSNumber *quantity = [NSNumber numberWithInteger:[countArray[button.tag] integerValue]];
    NSNumber *price = meal.price;
    NSNumber *total1 = @([quantity floatValue] * [price floatValue]);
    
    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
    f.numberStyle = NSNumberFormatterDecimalStyle;
    NSNumber *total2 = [f numberFromString:_footerView.priceLabel.text];
    
    NSNumber *sum = [NSNumber numberWithFloat:([total1 floatValue] + [total2 floatValue])];
    
    NSNumberFormatter *fmt = [[NSNumberFormatter alloc] init];
    [fmt setPositiveFormat:@"0.##"];
    
    button.enabled = NO;
    [button setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    
    _footerView.priceLabel.text = [fmt stringFromNumber:sum];
    
    NSArray *keys = [[meal.entity attributesByName] allKeys];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithDictionary:[meal dictionaryWithValuesForKeys:keys]];
    dict[@"quantity"] = quantity;
    dict[@"amount"] = total1;
    [ordersArray addObject:dict];
    
    orderCount += 1;
}

#pragma mark - scroll methods
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if(self.tableView.contentOffset.y >= (self.tableView.contentSize.height - self.tableView.bounds.size.height)) {
        _footerView.hidden = YES;
    } else {
        if (orderCount > 0)
            _footerView.hidden = NO;
    }
}

#pragma mark - NSFetchedResultsControllerDelegate
- (NSFetchedResultsController *)fetchedResultsController
{
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"Meals" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSSortDescriptor *sort = [[NSSortDescriptor alloc]
                              initWithKey:@"title" ascending:NO];
    [fetchRequest setSortDescriptors:[NSArray arrayWithObject:sort]];

    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"categories == %@", _category];
    [fetchRequest setPredicate:predicate];
    
    NSFetchedResultsController *theFetchedResultsController =
    [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                        managedObjectContext:self.managedObjectContext sectionNameKeyPath:nil
                                                   cacheName:nil]; //@"Root"
    self.fetchedResultsController = theFetchedResultsController;
    _fetchedResultsController.delegate = self;
    
    NSError *error = nil;
    if(![self.fetchedResultsController performFetch:&error]){
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _fetchedResultsController;
}

#pragma mark - Segue
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(NSIndexPath *)sender
{
    if ([[segue identifier] isEqualToString:@"toOrders"]) {
        OrderViewController *vc = [segue destinationViewController];
        vc.ordersArray = ordersArray;
    }
}

@end
