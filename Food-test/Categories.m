//
//  Categories.m
//  Food-test
//
//  Created by Eugeniya Pervushina on 18/5/16.
//  Copyright © 2016 Air. All rights reserved.
//

#import "Categories.h"

@implementation Categories

@dynamic category;
@dynamic image_name;

@end
