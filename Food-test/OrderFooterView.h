//
//  OrderFooterView.h
//  Food-test
//
//  Created by Eugeniya Pervushina on 17/5/16.
//  Copyright © 2016 Air. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderFooterView : UIView

@property (nonatomic, weak) IBOutlet UILabel *priceLabel;
@property (nonatomic, weak) IBOutlet UIButton *orderButton;

@end
