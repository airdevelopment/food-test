//
//  OrderTVCell.h
//  Food-test
//
//  Created by Eugeniya Pervushina on 17/5/16.
//  Copyright © 2016 Air. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderTVCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIImageView *foodImageView;
@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UILabel *priceLabel;
@property (nonatomic, weak) IBOutlet UILabel *countLabel;
@property (nonatomic, weak) IBOutlet UIButton *plusButton;
@property (nonatomic, weak) IBOutlet UIButton *minusButton;

@property (nonatomic, weak) IBOutlet UILabel *amountPriceLabel;
@property (nonatomic, weak) IBOutlet UIButton *deleteButton;

@end
