//
//  OrderViewController.m
//  Food-test
//
//  Created by Eugeniya Pervushina on 17/5/16.
//  Copyright © 2016 Air. All rights reserved.
//

#import "OrderViewController.h"
#import "OrderTVCell.h"
#import "TotalTVCell.h"
#import "OrderFooterView.h"

@interface OrderViewController () <UITableViewDelegate, UITableViewDataSource> {
    NSMutableArray *countArray;
}

@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic) IBOutlet OrderFooterView *footerView;

@end

@implementation OrderViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TableView
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _ordersArray.count + 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == _ordersArray.count) {
        TotalTVCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TotalCell"];
        
        if (cell == nil)
        {
            cell = [[TotalTVCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:@"TotalCell"];
        }
        
        NSNumber *sum;
        for (int i = 0; i < _ordersArray.count; i++) {
            NSDictionary *dict = _ordersArray[i];
            sum = [NSNumber numberWithFloat:[sum floatValue] + [dict[@"amount"] floatValue]];
        }
        NSNumberFormatter *fmt = [[NSNumberFormatter alloc] init];
        [fmt setPositiveFormat:@"0.##"];
        
        cell.totalLabel.text = [fmt stringFromNumber:sum];
        _footerView.priceLabel.text = [fmt stringFromNumber:sum];
        
        return cell;
    }
    
    OrderTVCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    if (cell == nil)
    {
        cell = [[OrderTVCell alloc] initWithStyle:UITableViewCellStyleDefault
                                 reuseIdentifier:@"Cell"];
    }
    NSDictionary *dict = _ordersArray[indexPath.row];
    cell.titleLabel.text = dict[@"title"];
    cell.foodImageView.image = [UIImage imageNamed:dict[@"image_name"]];
    
    NSNumber *price = dict[@"price"];
    cell.priceLabel.text = [NSString stringWithFormat:@"%@", price];
    
    NSNumber *quantity = dict[@"quantity"];
    cell.countLabel.text = [NSString stringWithFormat:@"%@", quantity];
    
    NSNumber *amount = dict[@"amount"];
    cell.amountPriceLabel.text = [NSString stringWithFormat:@"%@", amount];
    
    cell.plusButton.tag = indexPath.row;
    cell.minusButton.tag = indexPath.row;
    cell.deleteButton.tag = indexPath.row;
    [cell.plusButton addTarget:self action:@selector(plusButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [cell.minusButton addTarget:self action:@selector(minusButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [cell.deleteButton addTarget:self action:@selector(deleteButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == _ordersArray.count) {
        return 63;
    }
    return 107;
}

#pragma mark - buttons methods
- (void)plusButtonPressed:(UIButton *)button {
    NSMutableDictionary *dict = _ordersArray[button.tag];
    NSInteger num = [dict[@"quantity"] integerValue];
    num += 1;
    dict[@"quantity"] = [NSNumber numberWithInteger:num];
    NSNumber *total1 = @([[NSNumber numberWithInteger:num] floatValue] * [dict[@"price"] floatValue]);
    dict[@"amount"] = total1;
    [_ordersArray replaceObjectAtIndex:(NSUInteger)button.tag withObject:dict];
    [self.tableView reloadData];
}

- (void)minusButtonPressed:(UIButton *)button {
    NSMutableDictionary *dict = _ordersArray[button.tag];
    NSInteger num = [dict[@"quantity"] integerValue];
    
    if (num > 0) {
        num -= 1;
        dict[@"quantity"] = [NSNumber numberWithInteger:num];
        NSNumber *total1 = @([[NSNumber numberWithInteger:num] floatValue] * [dict[@"price"] floatValue]);
        dict[@"amount"] = total1;
        [_ordersArray replaceObjectAtIndex:(NSUInteger)button.tag withObject:dict];
        [self.tableView reloadData];
    }
}

- (void)deleteButtonPressed:(UIButton *)button {
    NSInteger index = button.tag;
    [_ordersArray removeObjectAtIndex:index];
    [self.tableView reloadData];
}

@end
