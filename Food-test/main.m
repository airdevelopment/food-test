//
//  main.m
//  Food-test
//
//  Created by Eugeniya Pervushina on 16/5/16.
//  Copyright © 2016 Air. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
