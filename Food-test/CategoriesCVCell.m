//
//  CategoriesCVCell.m
//  Food-test
//
//  Created by Eugeniya Pervushina on 17/5/16.
//  Copyright © 2016 Air. All rights reserved.
//

#import "CategoriesCVCell.h"

@implementation CategoriesCVCell

- (void)layoutSubviews
{
    [super layoutSubviews];
    self.borderView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.borderView.layer.borderWidth = 1.0f;
}

@end
