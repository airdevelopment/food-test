//
//  MealCVCell.m
//  Food-test
//
//  Created by Eugeniya Pervushina on 17/5/16.
//  Copyright © 2016 Air. All rights reserved.
//

#import "MealTVCell.h"

@implementation MealTVCell

- (void)layoutSubviews
{
    [super layoutSubviews];
    self.orderButton.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.orderButton.layer.borderWidth = 1.0f;
}

@end
